/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Code.Addict
 */
public class UserService {
	
	private static ArrayList<User> listData = new ArrayList<>();
	private static User currentUser = null;
	private static User defaultOwnerAccount = new User("root", "pass");

	// Array of Users
	static{
		load();
	}
	
	// + Add 
	static boolean addUser(User user){
		listData.add(user);
		save();
		return true;
	}

	
	// - Remove
	static boolean removeUser(User user){
		listData.remove(user);
		save();
		return true;
	}
	static boolean removeUser(int index){
		listData.remove(index);
		save();
		return true;
	}

	// * Update
	static boolean updateUser(int index, User user){
		listData.set(index, user);
		save();
		return true;
	}

	static ArrayList<User> getUser(){
		return listData;
	}
	static User getUser(int index){
		return listData.get(index);
	}
	static User getCurrentUser(){
		return currentUser;
	}

	// @ Save & Load Data
	static void save(){

		File file = null;
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		try {
			file = new File("userDatabase.bin");
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(listData);
			oos.close();
			fos.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	static void load(){

		File file = null;
		FileInputStream fis = null;
		ObjectInputStream ois = null;

		try {
			file = new File("userDatabase.bin");
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);
			listData = (ArrayList<User>)ois.readObject();
			ois.close();
			fis.close();
		} catch (FileNotFoundException ex) {
			Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
		}
    }
	
	private static boolean isOwner(String user, String pass){
		if(defaultOwnerAccount.getUsername().equals(user) && defaultOwnerAccount.getPassword().equals(pass)){
			return true;
		}
		return false;
	}

	static User secure(String user, String pass){
		if(isOwner(user, pass)){
			currentUser = defaultOwnerAccount;
			return defaultOwnerAccount;
		}
		
		for(int index = 0; index < listData.size(); index++){
			User pulledUser = listData.get(index);
			if( pulledUser.getUsername().equals(user) && pulledUser.getPassword().equals(pass) ){
				currentUser = pulledUser;
				return pulledUser;
			}
		}

		return null;
	}

	static void logout(){
		currentUser = null;
	}

	
	

}
