/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.usermanagement;

import java.io.Serializable;

/**
 *
 * @author Code.Addict
 */
public class User implements Serializable{

	String username, password;

	User(String username, String password){
		this.username = username;
		this.password = password;
	}
	
	String getUsername(){
		return this.username;
	}
	String getPassword(){
		return this.password;
	}

	@Override
	public String toString() {
		return "User:" + this.username + " & Password:" + this.password;
	}
	
}
